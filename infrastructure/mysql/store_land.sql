-- DROP DATABASE STORE_LAND;
-- CREATE DATABASE STORE_LAND CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE STORE_LAND;

CREATE TABLE `USER` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	full_name VARCHAR(100),
	avatar_url VARCHAR(500),
	email VARCHAR(100) NOT NULL,
	password VARCHAR(300) NOT NULL,
	is_admin BOOLEAN NOT NULL DEFAULT 0,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	UNIQUE (username),
	UNIQUE (email)
);

CREATE TABLE `STORE` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	slug VARCHAR(100) NOT NULL,
	website_url VARCHAR(100) NOT NULL,
	logo_url VARCHAR(100) NOT NULL,
	icon_url VARCHAR(100) NOT NULL,
	contact_address VARCHAR(300),
	contact_phone VARCHAR(15),
	contact_email VARCHAR(100),
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	UNIQUE (slug),
	CONSTRAINT `fk_sls_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_sls_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `CATEGORY` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	slug VARCHAR(100) NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	UNIQUE (slug),
	CONSTRAINT `fk_cat_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_cat_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `CATEGORY_MAP` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	parent_id INT UNSIGNED NOT NULL,
	child_id INT UNSIGNED NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	CONSTRAINT full_category UNIQUE (parent_id, child_id),
	CONSTRAINT `fk_catmap_cat_1` FOREIGN KEY (parent_id) REFERENCES `CATEGORY` (id),
	CONSTRAINT `fk_catmap_cat_2` FOREIGN KEY (child_id) REFERENCES `CATEGORY` (id),
	CONSTRAINT `fk_catmap_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_catmap_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `PRODUCT` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    store_sku VARCHAR(500) NOT NULL,
	title VARCHAR(500) NOT NULL,
	url VARCHAR(500) NOT NULL,
	image_url VARCHAR(500) NOT NULL,
	slug VARCHAR(500) NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	UNIQUE (store_sku),
	UNIQUE (slug),
	CONSTRAINT `fk_pro_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_pro_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `STORE_PRODUCT` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	store_id INT UNSIGNED NOT NULL,
	product_id INT UNSIGNED NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	CONSTRAINT `fk_srp_str` FOREIGN KEY (store_id) REFERENCES `STORE` (id),
	CONSTRAINT `fk_srp_pro` FOREIGN KEY (product_id) REFERENCES `PRODUCT` (id),
	CONSTRAINT `fk_srp_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_srp_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `PRODUCT_HISTORY` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	product_id INT UNSIGNED NOT NULL,
	price DECIMAL(12, 2) NOT NULL,
	price_old DECIMAL(12, 2),
	has_stock BOOLEAN,
	stock_qty INT UNSIGNED,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	CONSTRAINT `fk_prh_pro` FOREIGN KEY (product_id) REFERENCES `PRODUCT` (id),
	CONSTRAINT `fk_prh_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id)
);

CREATE TABLE `PRODUCT_CATEGORY` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	product_id INT UNSIGNED NOT NULL,
	category_map_id INT UNSIGNED NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	CONSTRAINT `fk_prc_pro` FOREIGN KEY (product_id) REFERENCES `PRODUCT` (id),
	CONSTRAINT `fk_prc_catmap` FOREIGN KEY (category_map_id) REFERENCES `CATEGORY_MAP` (id),
	CONSTRAINT `fk_prc_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_prc_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `LU_VISIT_SOURCE` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	slug VARCHAR(100) NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,

	PRIMARY KEY (id),
	CONSTRAINT `fk_lvs_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id)
);

CREATE TABLE `PRODUCT_VISIT` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	product_id INT UNSIGNED NOT NULL,
	source_id INT UNSIGNED NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,

	PRIMARY KEY (id),
	CONSTRAINT `fk_prvi_pro` FOREIGN KEY (product_id) REFERENCES `PRODUCT` (id),
	CONSTRAINT `fk_prvi_lvs` FOREIGN KEY (source_id) REFERENCES `LU_VISIT_SOURCE` (id),
	CONSTRAINT `fk_prvi_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id)
);

CREATE TABLE `STORE_CATEGORY` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	store_id INT UNSIGNED NOT NULL,
	category_map_id INT UNSIGNED NOT NULL,
	url VARCHAR(500) NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	UNIQUE (url),
	CONSTRAINT `fk_src_str` FOREIGN KEY (store_id) REFERENCES `STORE` (id),
	CONSTRAINT `fk_src_catmap` FOREIGN KEY (category_map_id) REFERENCES `CATEGORY_MAP` (id),
	CONSTRAINT `fk_src_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_src_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

CREATE TABLE `PRODUCT_VOTE` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	product_id INT UNSIGNED NOT NULL,
	up_vote BOOLEAN NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	CONSTRAINT `fk_prv_pro` FOREIGN KEY (product_id) REFERENCES `PRODUCT` (id),
	CONSTRAINT `fk_prv_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id)
);

CREATE TABLE `PRODUCT_COMMENT` (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	product_id INT UNSIGNED NOT NULL,
	comment VARCHAR(2000) NOT NULL,
	
	created_by INT UNSIGNED NOT NULL,
	created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	updated_by INT UNSIGNED,
	updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_active BOOLEAN NOT NULL DEFAULT 1,
	
	PRIMARY KEY (id),
	CONSTRAINT `fk_prco_pro` FOREIGN KEY (product_id) REFERENCES `PRODUCT` (id),
	CONSTRAINT `fk_prco_usr_1` FOREIGN KEY (created_by) REFERENCES `USER` (id),
	CONSTRAINT `fk_prco_usr_2` FOREIGN KEY (updated_by) REFERENCES `USER` (id)
);

INSERT INTO `USER` (username, full_name, email, password, is_admin, created_by)
VALUES ('core-api', 'Core API', 'core.api@store-land.net', '$2b$12$TAvZ1n4rLtqQRF9MZEqIhu19C8Hz7rl7qR.zodC77o4QfVOEyF916', TRUE, 1),
       ('scraper-lambda', 'Scraper Lambda', 'scraper.lambda@store-land.net', '$2b$12$3C5Qr5Ufv5/UYy/nXOABi.qaRgjz5TgI8UFldtcgm2.se76HfqyR6', TRUE, 1),
       ('admin-user', 'Admin User', 'admin@store-land.net', '$2b$12$c1piIsFz6vaOUWGblEj4Ium6glGhNbsWzjIab7zbF5lnq0AZcgB/i', TRUE, 1);

INSERT INTO `STORE` (name, slug, website_url, logo_url, icon_url, contact_address, contact_phone, contact_email, created_by)
VALUES ('DD Tech', 'ddtech', 'https://ddtech.mx/', 'https://ddtech.mx/assets/img/ddtech.png', 'https://ddtech.mx/assets/img/ico152px.png', 'Calle Jose María Castilleros 3415 Col. Lomas de Polanco, CP. 44960 Guadalajara, Jalisco, México', '3315232823', 'ventas@ddtech.mx', 1),
       ('Cyberpuerta', 'cyberpuerta', 'https://www.cyberpuerta.mx/', 'https://www.cyberpuerta.mx/out/cyberpuertaV5/img/logo2.png', 'https://www.cyberpuerta.mx/out/cyberpuertaV5/img/favicon.ico', 'Avenida Chapultepec, Torre Chapultepec 15-piso 26, Lafayette, 44600 Guadalajara, Jal.', '3347371360', 'info@cyberpuerta.mx', 1);

INSERT INTO `CATEGORY` (name, slug, created_by)
VALUES ('Computadoras', 'computadoras', 1),
('Componentes', 'componentes', 1),
('Celulares y tablets', 'celulares-y-tablets', 1),
('Apple', 'apple', 1),
('Consolas y videojuegos', 'consolas-y-videojuegos', 1),
('Pantallas y monitores', 'pantallas-y-monitores', 1),
('Servidores', 'servidores', 1);

INSERT INTO `CATEGORY` (name, slug, created_by) VALUES
('Escritorio', 'escritorio', 1),
('Laptops', 'laptops', 1),
('Procesadores', 'procesadores', 1),
('Tarjetas de video', 'tarjetas-de-video', 1),
('Memorias RAM', 'memorias-ram', 1),
('Tarjetas madre', 'tarjetas-madre', 1),
('Unidades de Estado Sólido (SSD)', 'unidades-de-estado-solido-ssd', 1),
('Discos Duros (HDD)', 'discos-duros-hdd', 1),
('Fuentes de Poder', 'fuentes-de-poder', 1),
('Gabinetes', 'gabinetes', 1),
('Monitores', 'monitores', 1),
('Kits de Actualización', 'kits-de-actualizacion', 1),
('Refrigeración', 'refrigeracion', 1),
('Tarjetas de expansión', 'tarjetas-de-expansion', 1),
('Unidad de CD/DVD', 'unidad-de-cd-dvd', 1),
('Celulares', 'celulares', 1),
('Tablets', 'tablets', 1),
('Fundas', 'fundas', 1),
('Accesorios para celulares', 'accesorios-para-celulares', 1),
('Accesorios para tablets', 'accesorios-para-tablets', 1),
('Cargadores', 'cargadores', 1),
('iPhone', 'iphone', 1),
('iPad', 'ipad', 1),
('iPod', 'ipod', 1),
('Mac', 'mac', 1),
('Audio', 'audio', 1),
('Apple TV', 'apple-tv', 1),
('Apple Watch', 'apple-watch', 1),
('Mouse, Teclados y Trackpads', 'mouse-teclados-y-trackpads', 1),
('Accesorios', 'accesorios', 1),
('Consolas', 'consolas', 1),
('Juegos', 'juegos', 1),
('Controles', 'controles', 1),
('Servidores de Rack', 'servidores-de-rack', 1),
('Servidores de Torre', 'servidores-de-torre', 1),
('Servidores NAS', 'servidores-nas', 1),
('Servidores de Impresión', 'servidores-de-impresion', 1),
('Almacenamiento', 'almacenamiento', 1),
('Pantallas', 'pantallas', 1),
('Smart TV Box', 'smart-tv-box', 1),
('Reproductores', 'reproductores', 1),
('Home Theater', 'home-theater', 1),
('Decodificadores', 'decodificadores', 1),
('Proyectores', 'proyectores', 1),
('Soportes', 'soportes', 1);

INSERT INTO `CATEGORY_MAP` (parent_id, child_id, created_by) VALUES
(1, 8, 1), -- ('Escritorio', 'escritorio', 1),
(1, 9, 1), -- ('Laptops', 'laptops', 1),
(2, 10, 1), -- ('Procesadores', 'procesadores', 1),
(2, 11, 1), -- ('Tarjetas de video', 'tarjetas-de-video', 1),
(2, 12, 1), -- ('Memorias RAM', 'memorias-ram', 1),
(2, 13, 1), -- ('Tarjetas madre', 'tarjetas-madre', 1),
(2, 14, 1), -- ('Unidades de Estado Sólido (SSD)', 'unidades-de-estado-solido-ssd', 1),
(2, 15, 1), -- ('Discos Duros (HDD)', 'discos-duros-hdd', 1),
(2, 16, 1), -- ('Fuentes de Poder', 'fuentes-de-poder', 1),
(2, 17, 1), -- ('Gabinetes', 'gabinetes', 1),
(2, 18, 1), -- ('Monitores', 'monitores', 1),
(2, 19, 1), -- ('Kits de Actualización', 'kits-de-actualizacion', 1),
(2, 20, 1), -- ('Refrigeración', 'refrigeracion', 2, 1),
(2, 21, 1), -- ('Tarjetas de expansión', 'tarjetas-de-expansion', 1),
(2, 22, 1), -- ('Unidad de CD/DVD', 'unidad-de-cd-dvd', 1),
(3, 23, 1), -- ('Celulares', 'celulares', 1),
(3, 24, 1), -- ('Tablets', 'tablets', 1),
(3, 25, 1), -- ('Fundas', 'fundas', 1),
(3, 26, 1), -- ('Accesorios para celulares', 'accesorios-para-celulares', 1),
(3, 27, 1), -- ('Accesorios para tablets', 'accesorios-para-tablets', 1),
(3, 28, 1), -- ('Cargadores', 'cargadores', 1),
(4, 29, 1), -- ('iPhone', 'iphone', 1),
(4, 30, 1), -- ('iPad', 'ipad', 1),
(4, 31, 1), -- ('iPod', 'ipod', 1),
(4, 32, 1), -- ('Mac', 'mac', 1),
(4, 33, 1), -- ('Audio', 'audio', 1),
(4, 34, 1), -- ('Apple TV', 'apple-tv', 1),
(4, 35, 1), -- ('Apple Watch', 'apple-watch', 1),
(4, 18, 1), -- ('Monitores', 'monitores', 1),
(4, 36, 1), -- ('Mouse, Teclados y Trackpads', 'mouse-teclados-y-trackpads', 1),
(4, 37, 1), -- ('Accesorios', 'accesorios', 1),
(5, 38, 1), -- ('Consolas', 'consolas', 1),
(5, 39, 1), -- ('Juegos', 'juegos', 1),
(5, 40, 1), -- ('Controles', 'controles', 1),
(5, 37, 1), -- ('Accesorios', 'accesorios', 1),
(6, 41, 1), -- ('Servidores de Rack', 'servidores-de-rack', 1),
(6, 42, 1), -- ('Servidores de Torre', 'servidores-de-torre', 1),
(6, 43, 1), -- ('Servidores NAS', 'servidores-nas', 1),
(6, 44, 1), -- ('Servidores de Impresión', 'servidores-de-impresion', 1),
(6, 45, 1), -- ('Almacenamiento', 'almacenamiento', 1),
(6, 37, 1), -- ('Accesorios', 'accesorios', 1),
(7, 46, 1), -- ('Pantallas', 'pantallas', 1),
(7, 47, 1), -- ('Smart TV Box', 'smart-tv-box', 1),
(7, 48, 1), -- ('Reproductores', 'reproductores', 1),
(7, 49, 1), -- ('Home Theater', 'home-theater', 1),
(7, 50, 1), -- ('Decodificadores', 'decodificadores', 1),
(7, 51, 1), -- ('Proyectores', 'proyectores', 1),
(7, 52, 1); -- ('Soportes', 'soportes', 1);

INSERT INTO `STORE_CATEGORY` (store_id, category_map_id, url, created_by)
VALUES (1, 1, 'https://ddtech.mx/productos/computadoras/escritorio', 1),
       (1, 2, 'https://ddtech.mx/productos/computadoras/portatiles', 1),
       (1, 3, 'https://ddtech.mx/productos/componentes/procesadores', 1),
       (1, 4, 'https://ddtech.mx/productos/componentes/tarjetas-de-video', 1),
       (2, 1, 'https://www.cyberpuerta.mx/Computadoras/PC-s-de-Escritorio/', 1),
       (2, 2, 'https://www.cyberpuerta.mx/Computadoras/Laptops/', 1),
       (2, 3, 'https://www.cyberpuerta.mx/Computo-Hardware/Componentes/Procesadores/Procesadores-para-PC/', 1),
       (2, 4, 'https://www.cyberpuerta.mx/Computo-Hardware/Componentes/Tarjetas-de-Video/', 1);

INSERT INTO `LU_VISIT_SOURCE` (name, slug, created_by)
VALUES ('Web', 'web', 1),
       ('Android', 'android', 1),
       ('iOS', 'ios', 1);
