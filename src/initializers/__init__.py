from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
ma = Marshmallow()


def initialize_ma(app):
    ma.init_app(app)


def initialize_db(app):
    db.init_app(app)
