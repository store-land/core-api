from src.initializers import db
from src.models.user import UserModel


def get_user_by_email(email: str) -> UserModel:
    return db.session.query(UserModel).filter(UserModel.email == email).first()


def get_user_by_id(id: int) -> UserModel:
    return db.session.query(UserModel).get(id)
