from typing import Optional

from src.initializers import db
from src.models.store_land import CategoryMapModel


def get_category_map_by_parent_n_child_id(
    parent_id: int, child_id: int
) -> Optional[CategoryMapModel]:
    return (
        db.session.query(CategoryMapModel)
        .filter(
            CategoryMapModel.parent_id == parent_id,
            CategoryMapModel.child_id == child_id,
        )
        .first()
    )


def get_category_map_by_id(id: int) -> Optional[CategoryMapModel]:
    return db.session.query(CategoryMapModel).get(id)
