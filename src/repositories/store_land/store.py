from typing import List

from src.initializers import db
from src.models.store_land import StoreModel


def get_store_by_slug(slug: str) -> StoreModel:
    return db.session.query(StoreModel).filter(StoreModel.slug == slug).first()


def get_store_by_id(id: int) -> StoreModel:
    return db.session.query(StoreModel).get(id)


def get_stores() -> List[StoreModel]:
    return db.session.query(StoreModel).filter(StoreModel.is_active == 1).all()
