from typing import List

from sqlalchemy import and_, func

from src.initializers import db
from src.models.store_land import (
    ProductCategoryModel,
    ProductHistoryModel,
    ProductModel,
    StoreProductModel,
)


def get_store_category_product(store_id: int, category_map_id: int) -> List:
    subq = (
        db.session.query(
            ProductHistoryModel.product_id,
            func.max(ProductHistoryModel.created_date).label("created_date"),
        )
        .filter(ProductHistoryModel.is_active == 1)
        .group_by(
            ProductHistoryModel.product_id,
        )
        .subquery("t2")
    )

    return (
        db.session.query(
            ProductModel.id.label("product_id"),
            ProductModel.title,
            ProductModel.slug,
            ProductModel.url,
            ProductModel.image_url,
            ProductHistoryModel.id.label("product_history_id"),
            ProductHistoryModel.price,
            ProductHistoryModel.price_old,
            ProductHistoryModel.has_stock,
            ProductHistoryModel.stock_qty,
            ProductHistoryModel.created_date,
        )
        .join(subq, subq.c.product_id == ProductModel.id)
        .join(
            ProductHistoryModel,
            and_(
                ProductHistoryModel.product_id == ProductModel.id,
                ProductHistoryModel.created_date == subq.c.created_date,
            ),
        )
        .join(
            ProductCategoryModel,
            and_(
                ProductCategoryModel.product_id == ProductModel.id,
                ProductCategoryModel.category_map_id == category_map_id,
                ProductCategoryModel.is_active == 1,
            ),
        )
        .join(
            StoreProductModel,
            and_(
                StoreProductModel.product_id == ProductModel.id,
                StoreProductModel.store_id == store_id,
                StoreProductModel.is_active == 1,
            ),
        )
        .filter(ProductModel.is_active == 1)
        .all()
    )
