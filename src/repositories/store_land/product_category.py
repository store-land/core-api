from typing import List

from src.initializers import db
from src.models.store_land import ProductCategoryModel


def get_products_category_by_product_ids(
    product_ids: List[int], category_map_id: int
) -> List[ProductCategoryModel]:
    return (
        db.session.query(ProductCategoryModel)
        .filter(
            ProductCategoryModel.product_id.in_(product_ids),
            ProductCategoryModel.category_map_id == category_map_id,
        )
        .all()
    )
