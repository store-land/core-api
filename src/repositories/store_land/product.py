from typing import List

from src.initializers import db
from src.models.store_land import ProductModel


def get_product_by_store_sku(store_sku: str) -> ProductModel:
    return (
        db.session.query(ProductModel)
        .filter(ProductModel.store_sku == store_sku)
        .first()
    )


def get_product_by_id(id: int) -> ProductModel:
    return db.session.query(ProductModel).get(id)


def get_products_by_skus(skus: List[str]) -> List[ProductModel]:
    return db.session.query(ProductModel).filter(ProductModel.store_sku.in_(skus)).all()


def get_products_by_ids(ids: List[int]) -> List[ProductModel]:
    return db.session.query(ProductModel).filter(ProductModel.id.in_(ids)).all()
