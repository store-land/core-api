from typing import Optional

from src.initializers import db
from src.models.store_land import CategoryModel


def get_category_by_slug(slug: str) -> Optional[CategoryModel]:
    return db.session.query(CategoryModel).filter(CategoryModel.slug == slug).first()


def get_category_by_id(id: int) -> Optional[CategoryModel]:
    return db.session.query(CategoryModel).get(id)
