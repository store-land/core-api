from typing import List

from sqlalchemy import and_

from src.initializers import db
from src.models.store_land import (
    CategoryMapModel,
    CategoryModel,
    StoreCategoryModel,
    StoreModel,
)


def get_categories_by_store_id(store_id: int) -> List:
    return (
        db.session.query(
            StoreCategoryModel.store_id,
            StoreCategoryModel.url,
            CategoryModel.id,
            CategoryModel.name,
            CategoryModel.slug,
            CategoryMapModel.parent_id,
            CategoryMapModel.id.label("category_map_id"),
        )
        .join(
            CategoryMapModel,
            and_(
                CategoryMapModel.id == StoreCategoryModel.category_map_id,
                CategoryMapModel.is_active == 1,
            ),
        )
        .join(
            CategoryModel,
            and_(
                CategoryModel.id == CategoryMapModel.child_id,
                CategoryModel.is_active == 1,
            ),
        )
        .filter(
            StoreCategoryModel.store_id == store_id, StoreCategoryModel.is_active == 1
        )
        .all()
    )


def get_store_category_by_url(url: int):
    return (
        db.session.query(StoreCategoryModel)
        .filter(
            StoreCategoryModel.url == url,
        )
        .first()
    )


def get_stores_categories() -> List:
    return (
        db.session.query(
            StoreCategoryModel.store_id,
            StoreModel.slug.label("store_slug"),
            StoreCategoryModel.url,
            CategoryMapModel.id.label("category_map_id"),
            CategoryMapModel.child_id.label("category_id"),
        )
        .join(
            CategoryMapModel,
            and_(
                CategoryMapModel.id == StoreCategoryModel.category_map_id,
                CategoryMapModel.is_active == 1,
            ),
        )
        .join(
            StoreModel,
            and_(
                StoreModel.id == StoreCategoryModel.store_id, StoreModel.is_active == 1
            ),
        )
        .filter(StoreCategoryModel.is_active == 1)
        .order_by(StoreCategoryModel.store_id.asc(), CategoryMapModel.id.asc())
        .all()
    )
