from typing import List

from src.initializers import db
from src.models.store_land import StoreProductModel


def get_store_products_by_product_ids(
    store_id: int, product_ids: List[int]
) -> List[StoreProductModel]:
    return (
        db.session.query(StoreProductModel)
        .filter(
            StoreProductModel.store_id == store_id,
            StoreProductModel.product_id.in_(product_ids),
        )
        .all()
    )
