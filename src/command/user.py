import datetime
from typing import Dict

from flask_jwt_extended import create_access_token, get_jwt_identity

from src.initializers import db
from src.models.user import UserModel
from src.repositories.user import get_user_by_email, get_user_by_id
from src.utils.errors import UnauthorizedError


def is_auth_user() -> int:
    return get_jwt_identity()


def user_admin_required(user_id: int) -> UserModel:
    user = get_user_by_id(user_id)
    if not user.is_admin:
        raise UnauthorizedError
    return user


def signup(user: UserModel, user_id: int) -> UserModel:
    user.hash_password()
    user.created_by = user_id

    db.session.add(user)
    db.session.commit()
    return user


def login(payload: Dict) -> str:
    user = get_user_by_email(payload["email"])

    if user is None or not user.is_active:
        raise UnauthorizedError

    authorized = user.check_password(payload["password"])
    if not authorized:
        raise UnauthorizedError

    expires = datetime.timedelta(days=7)
    return create_access_token(identity=str(user.id), expires_delta=expires)
