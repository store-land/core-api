from typing import Dict, List

from src.initializers import db
from src.models.store_land import ProductHistoryModel, ProductModel
from src.repositories.store_land.product import get_products_by_skus


def post_products(payload: List[Dict], user_id: int) -> List[ProductModel]:
    skus = list(map(lambda x: x["product"].store_sku, payload))
    existing_products = get_products_by_skus(skus)

    new_products = []
    linked = []
    for row in payload:
        payload_product: ProductModel = row["product"]
        product_history: ProductHistoryModel = row["product_history"]

        sku = payload_product.store_sku
        product = next((x for x in existing_products if x.store_sku == sku), None)
        if product is None:
            product = payload_product
            product.created_by_id = user_id
            new_products.append(product)
        linked.append({"product": product, "product_history": product_history})

    db.session.add_all(new_products)
    db.session.commit()

    new_products_history = []
    for row in linked:
        product: ProductModel = row["product"]
        product_history: ProductHistoryModel = row["product_history"]

        product_history.product_id = product.id
        product_history.created_by_id = user_id
        new_products_history.append(product_history)
    db.session.add_all(new_products_history)
    db.session.commit()

    return existing_products + new_products
