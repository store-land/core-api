from typing import Dict, List

from src.command.store_land.category_map import get_category_map
from src.command.store_land.store import get_store
from src.initializers import db
from src.models.store_land import ProductCategoryModel, StoreProductModel
from src.repositories.store_land.product import get_products_by_ids
from src.repositories.store_land.product_category import (
    get_products_category_by_product_ids,
)
from src.repositories.store_land.store_product import get_store_products_by_product_ids
from src.utils.errors import NotFoundError


def post_store_category_product(
    store_id: int, category_map_id: int, payload: Dict, user_id: int
) -> List[Dict]:
    get_store(store_id)
    get_category_map(category_map_id)

    products = get_products_by_ids(payload["product_ids"])
    if not products:
        raise NotFoundError("No products to relationate")

    existing_product_ids = list(map(lambda x: x.id, products))
    existing_products_category = get_products_category_by_product_ids(
        existing_product_ids, category_map_id
    )
    existing_store_products = get_store_products_by_product_ids(
        store_id, existing_product_ids
    )

    results = []
    to_add = []
    for product_id in existing_product_ids:
        product_category = next(
            (x for x in existing_products_category if x.product_id == product_id), None
        )
        if product_category is None:
            product_category = ProductCategoryModel()
            product_category.product_id = product_id
            product_category.category_map_id = category_map_id
            product_category.created_by_id = user_id
            to_add.append(product_category)

        store_product = next(
            (x for x in existing_store_products if x.product_id == product_id), None
        )
        if store_product is None:
            store_product = StoreProductModel()
            store_product.product_id = product_id
            store_product.store_id = store_id
            store_product.created_by_id = user_id
            to_add.append(store_product)
        results.append(
            {
                "product_id": product_id,
                "product_category": product_category,
                "store_product": store_product,
            }
        )

    db.session.add_all(to_add)
    db.session.commit()

    for row in results:
        row["product_category_id"] = row["product_category"].id
        row["store_product_id"] = row["store_product"].id

    return results
