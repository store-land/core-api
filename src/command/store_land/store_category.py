from typing import Dict, List

from src.command.store_land.store import get_store
from src.initializers import db
from src.models.store_land import CategoryModel, StoreCategoryModel
from src.models.store_land.category_map import CategoryMapModel
from src.repositories.store_land.category import get_category_by_slug
from src.repositories.store_land.category_map import (
    get_category_map_by_parent_n_child_id,
)
from src.repositories.store_land.store_category import (
    get_categories_by_store_id,
    get_store_category_by_url,
)
from src.utils.errors import NotFoundError


def get_store_categories(store_id: int) -> List:
    categories = get_categories_by_store_id(store_id)

    if not categories:
        raise NotFoundError(f"Categories not found for Store ID: {store_id}")

    results = []
    parents_ids = []
    for category in categories:
        parent_id = category.parent_id
        if not parent_id in parents_ids:
            results.append(
                {
                    "id": parent_id,
                    "url": category.url,
                    "name": category.name,
                    "slug": category.slug,
                    "sub_categories": [],
                }
            )
            parents_ids.append(parent_id)

    for parent in results:
        for category in categories:
            if parent["id"] == category.parent_id:
                parent["sub_categories"].append(
                    {
                        "category_map_id": category.category_map_id,
                        "id": category.id,
                        "url": category.url,
                        "name": category.name,
                        "slug": category.slug,
                    }
                )
    return results


def post_store_category(
    store_id: int, payload: Dict, user_id: int
) -> StoreCategoryModel:
    get_store(store_id)

    url = payload.pop("url")
    parent_id = payload.pop("parent_id")
    category = get_category_by_slug(payload["slug"])
    if not category:
        category = CategoryModel(**payload)
        category.created_by_id = user_id
        db.session.add(category)
        db.session.commit()

    category_map = get_category_map_by_parent_n_child_id(parent_id, category.id)
    if not category_map:
        category_map = CategoryMapModel()
        category_map.parent_id = parent_id
        category_map.child_id = category.id
        category_map.created_by_id = user_id
        db.session.add(category_map)
        db.session.commit()

    store_category = get_store_category_by_url(url)
    if not store_category:
        store_category = StoreCategoryModel()
        store_category.store_id = store_id
        store_category.category_map_id = category_map.id
        store_category.url = url
        store_category.created_by_id = user_id
        db.session.add(store_category)
        db.session.commit()

    return store_category
