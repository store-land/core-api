from src.models.store_land import CategoryModel
from src.repositories.store_land.category import get_category_by_id
from src.utils.errors import NotFoundError


def get_category(id: int) -> CategoryModel:
    category = get_category_by_id(id)
    if not category or not category.is_active:
        raise NotFoundError(f"Category not found with ID: {id}")
    return category
