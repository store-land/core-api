from src.initializers import db
from src.models.store_land import StoreModel
from src.repositories.store_land.store import get_store_by_id
from src.utils.errors import NotFoundError


def get_store(id: int) -> StoreModel:
    store = get_store_by_id(id)
    if not store or not store.is_active:
        raise NotFoundError(f"Store not found with ID: {id}")
    return store


def post_store(store: StoreModel, user_id: int) -> StoreModel:
    store.created_by_id = user_id

    db.session.add(store)
    db.session.commit()

    return store
