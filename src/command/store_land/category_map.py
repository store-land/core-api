from src.models.store_land import CategoryMapModel
from src.repositories.store_land.category_map import get_category_map_by_id
from src.utils.errors import NotFoundError


def get_category_map(id: int) -> CategoryMapModel:
    category_map = get_category_map_by_id(id)
    if not category_map or not category_map.is_active:
        raise NotFoundError(f"Category map not found with ID: {id}")
    return category_map
