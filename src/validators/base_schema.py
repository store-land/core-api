from marshmallow import Schema, fields, post_load

from src.models.base_model import base


class BaseSchema(Schema):
    __model__ = base

    @post_load
    def make_object(self, data, **kwargs):
        return self.__model__(**data)


class IdSchema(BaseSchema):
    id = fields.Integer()
