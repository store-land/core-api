from marshmallow import fields, validate

from src.models.store_land import ProductModel
from src.validators.base_schema import BaseSchema


class ProductSchema(BaseSchema):
    __model__ = ProductModel

    store_sku = fields.String(
        data_key="storeSku", required=True, validate=[validate.Length(min=1, max=500)]
    )
    title = fields.String(required=True, validate=[validate.Length(min=3, max=500)])
    url = fields.String(required=True, validate=[validate.Length(min=10, max=500)])
    image_url = fields.String(
        data_key="imageUrl", required=True, validate=[validate.Length(min=10, max=500)]
    )
    slug = fields.String(required=True, validate=[validate.Length(min=3, max=500)])
