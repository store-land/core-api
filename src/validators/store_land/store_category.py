from marshmallow import Schema, fields, validate


class GetCategoryResponse(Schema):
    category_map_id = fields.Integer(data_key="categoryMapId")
    id = fields.Integer()
    url = fields.String()
    name = fields.String()
    slug = fields.String()
    sub_categories = fields.List(
        fields.Nested(lambda: GetCategoryResponse()), data_key="subCategories"
    )


class PostStoreCategorySchema(Schema):
    url = fields.String(required=True, validate=[validate.Length(min=10, max=500)])
    name = fields.String(required=True, validate=[validate.Length(min=3, max=100)])
    slug = fields.String(required=True, validate=[validate.Length(min=3, max=100)])
    parent_id = fields.Integer(data_key="parentId", allow_none=True)


class GetStoreCategoryResponse(Schema):
    store_id = fields.Integer(data_key="storeId", required=True)
    store_slug = fields.String(data_key="storeSlug", required=True)
    url = fields.String(required=True)
    category_id = fields.Integer(data_key="categoryId", required=True)
    category_map_id = fields.Integer(data_key="categoryMapId")
