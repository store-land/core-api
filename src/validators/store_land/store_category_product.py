from marshmallow import Schema, fields

from src.validators.store_land.product import ProductSchema
from src.validators.store_land.product_history import ProductHistorySchema


class GetProductResponse(Schema):
    product_id = fields.Integer(data_key="productId", required=True)
    title = fields.String(required=True)
    slug = fields.String(required=True)
    url = fields.String(required=True)
    image_url = fields.String(data_key="imageUrl", required=True)
    product_history_id = fields.Integer(data_key="productHistoryId", required=True)
    price = fields.Float(required=True)
    price_old = fields.Float(data_key="priceOld")
    has_stock = fields.Boolean(data_key="hasStock", allow_none=True)
    stock_qty = fields.Integer(data_key="stockQty", allow_none=True)
    created_date = fields.Date(data_key="createdDate", required=True)


class PostProductResponse(Schema):
    product_id = fields.Integer(data_key="productId", required=True)
    product_category_id = fields.Integer(data_key="productCategoryId", required=True)
    store_product_id = fields.Integer(data_key="storeProductId", required=True)


class PostScrapingProductSchema(Schema):
    product = fields.Nested(ProductSchema, required=True)
    product_history = fields.Nested(
        ProductHistorySchema, data_key="productHistory", required=True
    )


class PostStoreIdCategoryIdProductIdSchema(Schema):
    product_ids = fields.List(fields.Integer, data_key="productIds", required=True)
