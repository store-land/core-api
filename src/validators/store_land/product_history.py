from marshmallow import fields

from src.models.store_land import ProductHistoryModel
from src.validators.base_schema import BaseSchema


class ProductHistorySchema(BaseSchema):
    __model__ = ProductHistoryModel

    price = fields.Float(required=True)
    price_old = fields.Float(data_key="priceOld")
    has_stock = fields.Boolean(data_key="hasStock", allow_none=True)
    stock_qty = fields.Integer(data_key="stockQty", allow_none=True)
