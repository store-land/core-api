from marshmallow import fields, validate

from src.models.store_land import StoreModel
from src.validators.base_schema import BaseSchema


class StoreSchema(BaseSchema):
    __model__ = StoreModel

    name = fields.String(required=True, validate=[validate.Length(min=3, max=100)])
    slug = fields.String(required=True, validate=[validate.Length(min=3, max=100)])
    website_url = fields.String(
        data_key="websiteUrl",
        required=True,
        validate=[validate.Length(min=10, max=100)],
    )
    logo_url = fields.String(
        data_key="logoUrl", required=True, validate=[validate.Length(min=10, max=100)]
    )
    icon_url = fields.String(
        data_key="iconUrl", required=True, validate=[validate.Length(min=10, max=100)]
    )
    contact_address = fields.String(
        data_key="contactAddress", validate=[validate.Length(min=10, max=300)]
    )
    contact_phone = fields.String(
        data_key="contactPhone", validate=[validate.Length(min=10, max=15)]
    )
    contact_email = fields.Email(
        data_key="contactEmail", validate=[validate.Length(min=10, max=100)]
    )


class GetStoreResponse(BaseSchema):
    __model__ = StoreModel

    id = fields.Integer()
    name = fields.String()
    slug = fields.String()
    website_url = fields.String(data_key="websiteUrl")
    logo_url = fields.String(data_key="logoUrl")
    icon_url = fields.String(data_key="iconUrl")
    contact_address = fields.String(data_key="contactAddress")
    contact_phone = fields.String(data_key="contactPhone")
    contact_email = fields.Email(data_key="contactEmail")
