from marshmallow import Schema, fields, validate

from src.models.user import UserModel
from src.validators.base_schema import BaseSchema


class UserValidator(BaseSchema):
    __model__ = UserModel

    username = fields.String(required=True, validate=[validate.Length(min=5, max=50)])
    full_name = fields.String(validate=[validate.Length(min=5, max=50)])
    avatar_url = fields.String(validate=[validate.Length(min=10, max=500)])
    email = fields.Email(required=True, validate=[validate.Length(min=10, max=100)])
    password = fields.String(required=True, validate=[validate.Length(min=8, max=100)])


class LoginValidator(Schema):
    email = fields.Email(required=True, validate=[validate.Length(min=10, max=100)])
    password = fields.String(required=True, validate=[validate.Length(min=8, max=100)])
