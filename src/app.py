import os

import sentry_sdk
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_restful import Api
from sentry_sdk.integrations.flask import FlaskIntegration

from src.initializers import initialize_db, initialize_ma
from src.utils import errors


def create():
    SENTRY_API = os.getenv("SENTRY_API")
    if SENTRY_API:
        sentry_sdk.init(
            dsn=os.getenv("SENTRY_API"),
            integrations=[FlaskIntegration()],
            traces_sample_rate=1.0,
        )

    app = Flask(__name__)
    app.config.from_pyfile("config.py")

    from src.routes.blueprint import initialize_routes

    api = Api(app, errors=errors)
    Bcrypt(app)
    JWTManager(app)

    initialize_db(app)
    initialize_ma(app)
    initialize_routes(api)

    return app
