from marshmallow import ValidationError
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.exceptions import BadRequestKeyError


class CoreException(Exception):
    def __init__(self, message=None):
        if message:
            self.default = message
        super().__init__(self.default)


class InternalServerError(CoreException):
    default = "Internal server error"


class UnauthorizedError(CoreException):
    default = "Unauthorized error"


class NotFoundError(CoreException):
    default = "NotFound error"


def get_error_structure(message: str):
    return {"message": message}


def get_error_message(error: Exception):
    if isinstance(error, ValidationError):
        return get_error_structure(str(error)), 400
    elif isinstance(error, NoResultFound):
        return get_error_structure(str(error)), 400
    elif isinstance(error, BadRequestKeyError):
        return get_error_structure(str(error)), 400
    elif isinstance(error, UnauthorizedError):
        return get_error_structure(str(error)), 401
    elif isinstance(error, NotFoundError):
        return get_error_structure(str(error)), 404

    return get_error_structure(str(error)), 500
