from flask_bcrypt import check_password_hash, generate_password_hash
from sqlalchemy import Boolean, Column, DateTime, Integer, String
from sqlalchemy.sql import func

from src.models.base_model import StoreLandModel


class UserModel(StoreLandModel):
    __tablename__ = "USER"

    id = Column(Integer, primary_key=True, nullable=False)
    username = Column(String(50), nullable=False)
    full_name = Column(String(100))
    avatar_url = Column(String(500))
    email = Column(String(100), nullable=False)
    password = Column(String(300), nullable=False)
    is_admin = Column(Boolean, nullable=False, default=False)

    created_by = Column(Integer, nullable=False)
    created_date = Column(DateTime, nullable=False, server_default=func.now())
    updated_by = Column(Integer)
    updated_date = Column(DateTime, onupdate=func.now())
    is_active = Column(Boolean, nullable=False, default=True)

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode("utf8")

    def check_password(self, password):
        return check_password_hash(self.password, password)
