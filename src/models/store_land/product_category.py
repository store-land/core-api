import os

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer
from sqlalchemy.orm import relationship

from src.models.base_model import StoreLandModel
from src.models.store_land import CategoryMapModel  # noqa
from src.models.store_land import ProductModel  # noqa
from src.models.user import UserModel  # noqa
from src.utils.general import get_datetime_now

database = os.getenv("MARIADB_DB")


class ProductCategoryModel(StoreLandModel):
    __tablename__ = "PRODUCT_CATEGORY"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    product_id = Column(Integer, ForeignKey(f"{database}.PRODUCT.id"), nullable=False)
    category_map_id = Column(
        Integer, ForeignKey(f"{database}.CATEGORY_MAP.id"), nullable=False
    )

    created_by_id = Column(
        "created_by", Integer, ForeignKey(f"{database}.USER.id"), nullable=False
    )
    created_date = Column(DateTime, nullable=False, default=get_datetime_now())
    updated_by_id = Column("updated_by", Integer, ForeignKey(f"{database}.USER.id"))
    updated_date = Column(DateTime, onupdate=get_datetime_now())
    is_active = Column(Boolean, nullable=False, default=True)

    product = relationship("ProductModel", foreign_keys=[product_id])
    category_map = relationship("CategoryMapModel", foreign_keys=[category_map_id])
    created_by = relationship("UserModel", foreign_keys=[created_by_id])
    updated_by = relationship("UserModel", foreign_keys=[updated_by_id])
