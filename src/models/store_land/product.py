import os

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from src.models.base_model import StoreLandModel
from src.models.user import UserModel  # noqa
from src.utils.general import get_datetime_now

database = os.getenv("MARIADB_DB")


class ProductModel(StoreLandModel):
    __tablename__ = "PRODUCT"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    store_sku = Column(String(500), nullable=False)
    title = Column(String(500), nullable=False)
    url = Column(String(500), nullable=False)
    image_url = Column(String(500), nullable=False)
    slug = Column(String(500), nullable=False)

    created_by_id = Column(
        "created_by", Integer, ForeignKey(f"{database}.USER.id"), nullable=False
    )
    created_date = Column(DateTime, nullable=False, default=get_datetime_now())
    updated_by_id = Column("updated_by", Integer, ForeignKey(f"{database}.USER.id"))
    updated_date = Column(DateTime, onupdate=get_datetime_now())
    is_active = Column(Boolean, nullable=False, default=True)

    created_by = relationship("UserModel", foreign_keys=[created_by_id])
    updated_by = relationship("UserModel", foreign_keys=[updated_by_id])
