import os

from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from src.models.base_model import StoreLandModel
from src.models.user import UserModel  # noqa
from src.utils.general import get_datetime_now

database = os.getenv("MARIADB_DB")


class StoreModel(StoreLandModel):
    __tablename__ = "STORE"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    name = Column(String(100), nullable=False)
    slug = Column(String(100), nullable=False)
    website_url = Column(String(100), nullable=False)
    logo_url = Column(String(100), nullable=False)
    icon_url = Column(String(100), nullable=False)
    contact_address = Column(String(300))
    contact_phone = Column(String(15))
    contact_email = Column(String(100))

    created_by_id = Column(
        "created_by", Integer, ForeignKey(f"{database}.USER.id"), nullable=False
    )
    created_date = Column(DateTime, nullable=False, default=get_datetime_now())
    updated_by_id = Column("updated_by", Integer, ForeignKey(f"{database}.USER.id"))
    updated_date = Column(DateTime, onupdate=get_datetime_now())
    is_active = Column(Boolean, nullable=False, default=True)

    created_by = relationship("UserModel", foreign_keys=[created_by_id])
    updated_by = relationship("UserModel", foreign_keys=[updated_by_id])
