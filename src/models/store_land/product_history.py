import os

from sqlalchemy import Boolean, Column, DateTime, Float, ForeignKey, Integer
from sqlalchemy.orm import relationship

from src.models.base_model import StoreLandModel
from src.models.store_land import ProductModel  # noqa
from src.models.user import UserModel  # noqa
from src.utils.general import get_datetime_now

database = os.getenv("MARIADB_DB")


class ProductHistoryModel(StoreLandModel):
    __tablename__ = "PRODUCT_HISTORY"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    product_id = Column(Integer, ForeignKey(f"{database}.PRODUCT.id"), nullable=False)
    price = Column(Float, nullable=False)
    price_old = Column(Float)
    has_stock = Column(Boolean)
    stock_qty = Column(Integer)

    created_by_id = Column(
        "created_by", Integer, ForeignKey(f"{database}.USER.id"), nullable=False
    )
    created_date = Column(DateTime, nullable=False, default=get_datetime_now())
    is_active = Column(Boolean, nullable=False, default=True)

    created_by = relationship("UserModel", foreign_keys=[created_by_id])
