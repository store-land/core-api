import os

from sqlalchemy.ext.declarative import declarative_base

base = declarative_base()


class StoreLandModel(base):
    __abstract__ = True
    __table_args__ = {"schema": os.getenv("MARIADB_DB")}
