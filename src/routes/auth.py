from typing import Dict

from flask import request

from src.command.user import login, signup
from src.routes.resource import CoreResource
from src.utils.constants import CORE_API_USER_ID
from src.validators.base_schema import IdSchema
from src.validators.user import LoginValidator, UserValidator


class SignupApi(CoreResource):
    def post(self) -> Dict:
        user = signup(UserValidator().load(request.get_json()), CORE_API_USER_ID)
        return IdSchema().dump(user)


class LoginApi(CoreResource):
    def post(self) -> Dict:
        access_token = login(LoginValidator().load(request.get_json()))
        return {"token": access_token}
