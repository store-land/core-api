from functools import wraps

from flask_restful import Resource

from src.utils.errors import get_error_message


def error_handling(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return result
        except Exception as e:
            result, status_code = get_error_message(e)
            return result, status_code

    return wrapper


class CoreResource(Resource):
    method_decorators = [error_handling]
