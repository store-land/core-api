from src.routes.auth import LoginApi, SignupApi
from src.routes.store_land import (
    ScrapingProductApi,
    StoreApi,
    StoreCategoryApi,
    StoreIdApi,
    StoreIdCategoryApi,
    StoreIdCategoryMapIdProductApi,
)


def initialize_routes(api):
    api.add_resource(SignupApi, "/api/auth/signup")
    api.add_resource(LoginApi, "/api/auth/login")

    api.add_resource(StoreApi, "/api/store")
    api.add_resource(StoreIdApi, "/api/store/<int:id>")

    api.add_resource(StoreIdCategoryApi, "/api/store/<int:store_id>/category")
    api.add_resource(StoreCategoryApi, "/api/store/category")
    api.add_resource(
        StoreIdCategoryMapIdProductApi,
        "/api/store/<int:store_id>/category-map/<int:category_map_id>/product",
    )
    api.add_resource(ScrapingProductApi, "/api/scraping/product")
