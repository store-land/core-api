from typing import Dict

from flask import request
from flask_jwt_extended import jwt_required

from src.command.store_land.product import post_products
from src.command.user import is_auth_user, user_admin_required
from src.routes.resource import CoreResource
from src.validators.store_land import PostScrapingProductSchema


class ScrapingProductApi(CoreResource):
    @jwt_required()
    def post(self) -> Dict:
        user_id = is_auth_user()
        user_admin_required(user_id)

        results = post_products(
            PostScrapingProductSchema(many=True).load(request.get_json()),
            user_id,
        )
        return {"productIds": list(map(lambda x: x.id, results))}
