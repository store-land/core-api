from typing import Dict

from flask import request
from flask_jwt_extended import jwt_required

from src.command.store_land.store_category_product import post_store_category_product
from src.command.user import is_auth_user, user_admin_required
from src.repositories.store_land.store_category_product import (
    get_store_category_product,
)
from src.routes.resource import CoreResource
from src.validators.store_land import (
    GetProductResponse,
    PostProductResponse,
    PostStoreIdCategoryIdProductIdSchema,
)


class StoreIdCategoryMapIdProductApi(CoreResource):
    def get(self, store_id: int, category_map_id: int) -> Dict:
        return GetProductResponse(many=True).dump(
            get_store_category_product(store_id, category_map_id)
        )

    @jwt_required()
    def post(self, store_id: int, category_map_id: int) -> Dict:
        user_id = is_auth_user()
        user_admin_required(user_id)

        return PostProductResponse(many=True).dump(
            post_store_category_product(
                store_id,
                category_map_id,
                PostStoreIdCategoryIdProductIdSchema().load(request.get_json()),
                user_id,
            )
        )
