from typing import Dict, List

from flask import request
from flask_jwt_extended import jwt_required

from src.command.store_land.store import get_store, post_store
from src.command.user import is_auth_user, user_admin_required
from src.repositories.store_land.store import get_stores
from src.routes.resource import CoreResource
from src.validators.base_schema import IdSchema
from src.validators.store_land import GetStoreResponse, StoreSchema


class StoreIdApi(CoreResource):
    def get(self, id: int) -> Dict:
        return GetStoreResponse().dump(get_store(id))


class StoreApi(CoreResource):
    def get(self) -> List[Dict]:
        return GetStoreResponse(many=True).dump(get_stores())

    @jwt_required()
    def post(self) -> Dict:
        user_id = is_auth_user()
        user_admin_required(user_id)

        store = post_store(StoreSchema().load(request.get_json()), user_id)
        return IdSchema().dump(store)
