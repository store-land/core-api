from typing import Dict, List

from flask import request
from flask_jwt_extended import jwt_required

from src.command.store_land.store_category import (
    get_store_categories,
    post_store_category,
)
from src.command.user import is_auth_user, user_admin_required
from src.repositories.store_land.store_category import get_stores_categories
from src.routes.resource import CoreResource
from src.validators.base_schema import IdSchema
from src.validators.store_land import (
    GetCategoryResponse,
    GetStoreCategoryResponse,
    PostStoreCategorySchema,
)


class StoreIdCategoryApi(CoreResource):
    def get(self, store_id: int) -> Dict:
        return GetCategoryResponse(many=True).dump(get_store_categories(store_id))

    @jwt_required()
    def post(self, store_id: int) -> Dict:
        user_id = is_auth_user()
        user_admin_required(user_id)

        return IdSchema().dump(
            post_store_category(
                store_id,
                PostStoreCategorySchema().load(request.get_json()),
                user_id,
            )
        )


class StoreCategoryApi(CoreResource):
    def get(self) -> List[Dict]:
        return GetStoreCategoryResponse(many=True).dump(get_stores_categories())
