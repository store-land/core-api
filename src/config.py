import os

SQLALCHEMY_TRACK_MODIFICATIONS = False
JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{os.getenv("MARIADB_USER")}:{os.getenv("MARIADB_PASS")}@{os.getenv("MARIADB_HOST")}:{os.getenv("MARIADB_PORT", 3306)}/{os.getenv("MARIADB_DB")}?charset=utf8mb4'
JSON_AS_ASCII = False
