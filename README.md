# Store Land Core API

## Description

This project is the basis for the Store Land systems, is the Core between all the services

Tech used:

- Python 3.9.2
- PipEnv
- Flask
- Gunicorn
- MariaDB
- Marshmallow
- SQLAlchemy
- Sentry

With this data we return analytics from the user who visit the shorten URL.

## Running in local

1. [Download and Install Python 3](https://www.python.org/downloads/).
2. Clone the project.
3. [Install PipEnv](https://pypi.org/project/pipenv/).
4. Create a pipenv project with ´pipenv --three´ inside the project root directory.
5. Now install all the dependencies with ´pipenv install´
6. Set the environment with ´pipenv shell´.
7. Edit the env variables (there is an example ´.env.example´, remove the ´.example´ part), you can leave empty the field of ´SENTRY_API´.
8. Now you can run the project with ´make start´
